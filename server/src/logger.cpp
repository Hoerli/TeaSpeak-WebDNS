#include "./logger.h"

#include <experimental/filesystem>
#include <spdlog/async.h>
#include <spdlog/sinks/daily_file_sink.h>
#include <spdlog/sinks/stdout_sinks.h>
#include <spdlog/sinks/stdout_color_sinks.h>

namespace fs = std::experimental::filesystem;

const static std::string log_folder{"logs/"};

std::shared_ptr<spdlog::details::thread_pool> log_thread{nullptr};

std::shared_ptr<spdlog::logger> logger::logger_general{nullptr};
std::shared_ptr<spdlog::logger> logger::logger_network{nullptr};
std::shared_ptr<spdlog::logger> logger::logger_dns{nullptr};

bool logger::setup(std::string& error) {
	try {
		if(!fs::exists(log_folder) && !fs::create_directories(log_folder)) {
			error = "failed to create directory " + log_folder;
			return false;
		}
	} catch(fs::filesystem_error& ex) {
		error = "failed to create log folder at " + log_folder + ": " + std::string{ex.what()};
		return false;
	}
	std::vector<spdlog::sink_ptr> sinks{};
	sinks.reserve(2);
	{
		auto file_sink = std::make_shared<spdlog::sinks::daily_file_sink_st>(log_folder + "log", 0, 0);
		file_sink->set_level(spdlog::level::trace);
		sinks.push_back(file_sink);

		auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_st>();
		console_sink->set_level(spdlog::level::trace);
		sinks.push_back(console_sink);
	}

	log_thread = std::make_shared<spdlog::details::thread_pool>(1024, 1);
	spdlog::register_logger(logger_general = std::make_shared<spdlog::async_logger>("general", std::begin(sinks), std::end(sinks), log_thread, spdlog::async_overflow_policy::block));
	spdlog::register_logger(logger_network = std::make_shared<spdlog::async_logger>("network", std::begin(sinks), std::end(sinks), log_thread, spdlog::async_overflow_policy::block));
	spdlog::register_logger(logger_dns     = std::make_shared<spdlog::async_logger>("dns    ", std::begin(sinks), std::end(sinks), log_thread, spdlog::async_overflow_policy::block));

	spdlog::apply_all([](const std::shared_ptr<spdlog::logger>& l) {
		l->set_level(spdlog::level::trace);
	});
	logger_dns->set_level(spdlog::level::info);
	spdlog::flush_every(std::chrono::seconds(5));
	return true;
}

bool logger::shutdown() {
	spdlog::drop_all();
	return true;
}