#pragma once

#include <memory>
#include <sys/socket.h>

namespace ts::dns {
	class DNSServerBinding;

	class DNSHandler {
		public:
			virtual void handle_message(const std::shared_ptr<DNSServerBinding>& /* binding */, const sockaddr_storage& /* address */, void* /* buffer */, size_t /* buffer size */) = 0;

		private:

	};

	class WebDNSHandler : public DNSHandler {
		public:
			void handle_message(const std::shared_ptr<DNSServerBinding>&, const sockaddr_storage &storage, void *pVoid, size_t size) override;

		private:
	};
}