#pragma once

#define SPDLOG_LEVEL_NAMES { "trace   ", "debug   ", "info    ",  "warning ", "error   ", "critical", "off     " }
#include <spdlog/spdlog.h>

namespace logger {
	extern std::shared_ptr<spdlog::logger> logger_general;
	extern std::shared_ptr<spdlog::logger> logger_network;
	extern std::shared_ptr<spdlog::logger> logger_dns;

	extern bool setup(std::string& /* error */);
	extern bool shutdown();
}

inline std::shared_ptr<spdlog::logger> log_general() { return logger::logger_general; }
inline std::shared_ptr<spdlog::logger> log_network() { return logger::logger_network; }
inline std::shared_ptr<spdlog::logger> log_dns() { return logger::logger_dns; }