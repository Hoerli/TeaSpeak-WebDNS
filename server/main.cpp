#include <iostream>
#include <cstring>
#include <event2/thread.h>
#include "src/logger.h"
#include "src/server.h"
#include "src/handler.h"

using namespace ts::dns;

std::vector<sockaddr_storage> bindings(uint16_t port) {
	std::vector<sockaddr_storage> result{};

	{
		sockaddr_in& any_v4{reinterpret_cast<sockaddr_in&>(result.emplace_back())};
		memset(&any_v4, 0, sizeof(sockaddr_in));

		any_v4.sin_family = AF_INET;
		any_v4.sin_port = htons(port);
		any_v4.sin_addr.s_addr = INADDR_ANY;
	}

	{
		sockaddr_in& any_v4{reinterpret_cast<sockaddr_in&>(result.emplace_back())};
		memset(&any_v4, 0, sizeof(sockaddr_in));

		any_v4.sin_family = AF_INET;
		any_v4.sin_port = htons(port);
		any_v4.sin_addr.s_addr = (1UL << 24U) | 127U;
	}

	return result;
}

extern std::vector<std::string> le_token;
int main(int argc, char** argv) {
	evthread_use_pthreads();
	std::string error{};

	if(!logger::setup(error)) {
		std::cerr << "Failed to setup log: " << error << "\n";
		return 1;
	}

	if(argc < 2) {
		log_general()->error("Missing port. Default DNS port is 53");
		return 1;
	}

	uint16_t port{0};
	try {
		port = std::stoul(argv[1]);
	} catch(std::exception& ex) {
		log_general()->error("Failed to parse port ({})", argv[1]);
		return 1;
	}

	auto handler = std::make_shared<WebDNSHandler>();
	DNSServer server{handler};

	log_general()->info("Starting server on port {}", port);
	if(!server.start(bindings(port), error)) {
		auto logger = log_general();
		for(auto& binding : server.bindings())
			logger->error(" - {}", error);
		logger->error("Failed to start server: {}", error);
		return 1;
	}

	std::string line;
	while(true) {
		std::getline(std::cin, line);
		if(line.empty())
			continue;

		if(line == "end" || line == "stop") {
			log_general()->info("Stopping server");
			break;
		} else if(line.length() > 13 && line.substr(0, 13) == "add-le-token ") {
			le_token.push_back(line.substr(13));
			log_general()->info("Added LetsEncrypt token {}", le_token.back());
		} else if(line.length() >= 14 && line.substr(0, 14) == "clear-le-token") {
			log_general()->info("Cleaning up {} le tokens", le_token.size());
			le_token.clear();
		} else if(line.length() >= 14 && line.substr(0, 14) == "list-le-token") {
			log_general()->info("LetsEncrypt tokens ({}):", le_token.size());
			for(auto& token : le_token)
				log_general()->info(" - ", token);
		} else {
			log_general()->error("Unknown command \"{}\"", line);
		}
	}

	server.stop();

	log_general()->info("Server stopped");
	logger::shutdown();
	libevent_global_shutdown();
	return 0;
}

//add-le-token dZS8bKQCTgQv62RE647sMlligbHhOGNTHyxNtYZkxtI